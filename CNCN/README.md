# Coffee and Cloud Native

Show notes and resources for Coffee and Cloud Native have moved to [a dedicated website](https://cncn.io).

Join the [Discord server](https://discord.gg/asBjrEu) and the [free community site](https://community.cncn.io) to see how the CNCN community can help you reach your goals.
